﻿using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

[GenerateAuthoringComponent]
public struct SpawnObstaclesConfigData : IComponentData
{
    public float SpawnTimerMin, SpawnTimerMax;
    public float PositionYMin, PositionYMax;
    public float MassMin, MassMax;
    public float PositionX;
    public int numberOfSizes;
    public Entity Obstacle;
}