﻿using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

[GenerateAuthoringComponent]
public struct GameConfigData : IComponentData
{
    public int NumberOfSwarm;
    public float Mass, Speed;
    public float KFactorToReduceSwarm;
    public Entity SingleElementSwarm;
    public Entity Player;
    [HideInInspector] public float Force;
}