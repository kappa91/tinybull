﻿using UnityEngine;
using System.Collections;
using Unity.Physics;
using Unity.Collections;
using Unity.Entities;
using Unity.Physics.Systems;
using Unity.Jobs;
using Unity.Burst;
using System.Collections.Generic;

[UpdateAfter(typeof(EndFramePhysicsSystem))]
public class ObstaclesTriggerSystem : SystemBase
{
    private BuildPhysicsWorld _buildPhysicsWorld;
    private StepPhysicsWorld _stepPhysicsWorld;
    private Entity _gameConfigEntity;
    private EntityCommandBufferSystem _buffer;

    private float _force;
    private int _currentNumberOfSwarm;
    private float _kFactor;
    private int _swarmNumber;

    [BurstCompile]
    struct ObstaclesTriggerJob : ITriggerEventsJob
    {
        [ReadOnly] public ComponentDataFromEntity<ObstacleTag> allObstacles;
        [ReadOnly] public ComponentDataFromEntity<SwarmTag> swarm;
        [ReadOnly] public ComponentDataFromEntity<ObstacleData> allObstaclesData;

        public Entity gameConfigEntity;
        public float kFactor;
        public float force;
        public int currentNumberOfSwarm;

        public NativeList<Entity> swarmList;
        public NativeArray<bool> isGameOver;

        public EntityCommandBuffer buffer;


        public void Execute(TriggerEvent triggerEvent)
        {
            //Check if trigger is between swarm and obstacles
            var swarmEntity = swarm.HasComponent(triggerEvent.EntityA) ? triggerEvent.EntityA :
                swarm.HasComponent(triggerEvent.EntityB) ? triggerEvent.EntityB : Entity.Null;

            var obstacleEntity = allObstacles.HasComponent(triggerEvent.EntityA) ? triggerEvent.EntityA :
               allObstacles.HasComponent(triggerEvent.EntityB) ? triggerEvent.EntityB : Entity.Null;

            if (swarmEntity == Entity.Null || obstacleEntity == Entity.Null) return;


       

            if (force > allObstaclesData[obstacleEntity].Mass)
            {
                buffer.DestroyEntity(obstacleEntity);

                //Calculate number(casted to int) to reduce swarm if the mass is > of swarm force
                int NumberToReduceSwarm = (int)(kFactor * allObstaclesData[obstacleEntity].Mass);

                if (NumberToReduceSwarm <= currentNumberOfSwarm)
                {
                    DestroySwarmNumberAndRecalculateForce(NumberToReduceSwarm);
                }
                else //swarm destroyed
                {
                    //Game Over - Restart Game
                    isGameOver[0] = true;
                }
            }
            else
            {
                //Game Over - Restart Game
                isGameOver[0] = true;
            }


        }


        void DestroySwarmNumberAndRecalculateForce(int numberToReduceSwarm)
        {
            //Destroy Swarm
            for (int i = 0; i < numberToReduceSwarm; i++)
            {
                buffer.DestroyEntity(swarmList[i]);
            }


            //Recalculate Force
            var newForce = (force / currentNumberOfSwarm) * (currentNumberOfSwarm - numberToReduceSwarm);
            buffer.SetComponent<GameConfigData>(gameConfigEntity, new GameConfigData
            {
                Force = newForce,
                NumberOfSwarm = currentNumberOfSwarm - numberToReduceSwarm
            });
        }

    }

    #region SystemFlow
    protected override void OnCreate()
    {
        _buildPhysicsWorld = World.GetOrCreateSystem<BuildPhysicsWorld>();
        _stepPhysicsWorld = World.GetOrCreateSystem<StepPhysicsWorld>();
        _buffer = World.GetOrCreateSystem<EntityCommandBufferSystem>();

        RequireSingletonForUpdate<GameConfigData>();
    }

    protected override void OnStartRunning()
    {
        GetGameConfig();
    }

    protected override void OnUpdate()
    {

        //Get bool is Game Over
        NativeArray<bool> _isGameOver = new NativeArray<bool>(1, Allocator.TempJob);
        _isGameOver[0] = false;


        //Get all single elements of Swarm (without player)
        NativeList<Entity> _swarmList = new NativeList<Entity>(_swarmNumber, Allocator.TempJob);
        Entities.WithAll<SingleElementSwarmTag>().ForEach((Entity entity) => {

            _swarmList.Add(entity);

        }).Schedule();

    var job = new ObstaclesTriggerJob
        {
            allObstacles = GetComponentDataFromEntity<ObstacleTag>(),
            allObstaclesData = GetComponentDataFromEntity<ObstacleData>(),
            swarm = GetComponentDataFromEntity<SwarmTag>(),
            buffer = _buffer.CreateCommandBuffer(),
            gameConfigEntity = _gameConfigEntity,
            swarmList = _swarmList,
            force = _force,
            currentNumberOfSwarm = _swarmNumber,
            kFactor = _kFactor,
            isGameOver = _isGameOver
        };

        Dependency = job.Schedule(_stepPhysicsWorld.Simulation, 
            ref _buildPhysicsWorld.PhysicsWorld, Dependency);

        _buffer.AddJobHandleForProducer(Dependency);

        Dependency.Complete();


        //Game over - Restart Game
        if (_isGameOver[0])
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("MainScene");
        }

        _swarmNumber = _swarmList.Length;

        _swarmList.Dispose();
        _isGameOver.Dispose();
    }
    #endregion


    private void GetGameConfig()
    {
        EntityQuery query = GetEntityQuery(typeof(GameConfigData));
        if (query == default) return;
        var gameConfigEntity= query.GetSingletonEntity();

        _gameConfigEntity = gameConfigEntity;
        _kFactor = GetComponent<GameConfigData>(gameConfigEntity).KFactorToReduceSwarm;
        _swarmNumber = GetComponent<GameConfigData>(gameConfigEntity).NumberOfSwarm;
        _force = GetComponent<GameConfigData>(gameConfigEntity).Force;
    }



}
