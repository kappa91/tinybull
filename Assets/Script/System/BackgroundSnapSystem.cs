﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

public class BackgroundSnapSystem : SystemBase
{
    protected override void OnUpdate()
    {
        Entities.WithAll<BackgroundTag>().ForEach((ref Translation translation) => {

            if (translation.Value.x <= -18)
            {
                translation.Value.x = 20;
            }

        }).Schedule();
    }
}
