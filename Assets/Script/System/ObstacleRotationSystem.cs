﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

public class ObstacleRotationSystem : SystemBase
{
    protected override void OnUpdate()
    {
        var deltaTime = Time.DeltaTime;

        Entities.WithAll<ObstacleTag>().ForEach((ref Rotation rotation) => {

            rotation.Value = math.mul(rotation.Value, quaternion.RotateZ(math.radians(50 * deltaTime)));

        }).Schedule();
    }
}
