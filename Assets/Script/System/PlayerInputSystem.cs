﻿using System;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

public class PlayerInputSystem : SystemBase
{
    private float maxSwipeTime = 0.5f;
    private float minSwipeDistance = 0.2f;

    private float verticalLimit = 3;
    private float horizontalLimit = 6;


    protected override void OnUpdate()
    {       

        Entities.WithoutBurst().ForEach((ref MoveData moveData, in InputData inputData, in Translation translation) => {

            var isUpKeyPressed = false;  
            var isDownKeyPressed = false;  
            var isLeftKeyPressed = false; 
            var isRightKeyPressed = false;

            //Swipe check
            var swipe = Swipe();

            //Move only inside screen
            //Y Check
            if (translation.Value.y + moveData.Speed <= verticalLimit)
            {
                isUpKeyPressed = SwipeUp(swipe) || Input.GetKeyDown(inputData.Up);
            }

            if (translation.Value.y - moveData.Speed >= -verticalLimit)
            {
                isDownKeyPressed = SwipeDown(swipe) || Input.GetKeyDown(inputData.Down);
            }


            //X Check
            if (translation.Value.x - moveData.Speed >= -horizontalLimit)
            {
                isLeftKeyPressed = SwipeLeft(swipe) || Input.GetKeyDown(inputData.Left);
            }

            if (translation.Value.x + moveData.Speed <= horizontalLimit)
            {
                isRightKeyPressed = SwipeDown(swipe) || Input.GetKeyDown(inputData.Right);
            }



            moveData.Direction.y = Convert.ToInt32(isUpKeyPressed);          
            moveData.Direction.y -= Convert.ToInt32(isDownKeyPressed);
            moveData.Direction.x = Convert.ToInt32(isRightKeyPressed);
            moveData.Direction.x -= Convert.ToInt32(isLeftKeyPressed);
            


        }).Run();
    }

    #region Swipe

    public bool SwipeDown(Vector2 swipe)
    {
        if (Mathf.Abs(swipe.x) < Mathf.Abs(swipe.y))
        {
            if (swipe.y < 0)
            {
                return true;
            }

        }
        return false;
    }

    public bool SwipeUp(Vector2 swipe)
    {
        if (Mathf.Abs(swipe.x) < Mathf.Abs(swipe.y))
        { 
            if (swipe.y > 0)
            {
                return true;
            }
   
        }
        return false;
    }

    public bool SwipeRight(Vector2 swipe)
    {
        if (Mathf.Abs(swipe.x) > Mathf.Abs(swipe.y))
        {
            if (swipe.x > 0)
            {
                return true;
            }

        }
        return false;
    }

    public bool SwipeLeft(Vector2 swipe)
    {
        if (Mathf.Abs(swipe.x) > Mathf.Abs(swipe.y))
        {
            if (swipe.x < 0)
            {
                return true;
            }

        }
        return false;
    }


    public Vector2 Swipe()
    {
        Vector2 startPos = Vector2.zero;
        float startTime = 0;


        if (Input.touches.Length > 0)
        {
            Touch t = Input.GetTouch(0);
            if (t.phase == TouchPhase.Began)
            {
                startPos = new Vector2(t.position.x / (float)Screen.width, t.position.y / (float)Screen.width);
                startTime = UnityEngine.Time.time;
            }
            if (t.phase == TouchPhase.Ended)
            {
                //cancel swiping because too long time has passed
                if (UnityEngine.Time.time - startTime > maxSwipeTime)
                    return Vector2.zero;

                Vector2 endPos = new Vector2(t.position.x / (float)Screen.width, t.position.y / (float)Screen.width);
                Vector2 swipe = new Vector2(endPos.x - startPos.x, endPos.y - startPos.y);

                //cancel swiping because it's a short swipe
                if (swipe.magnitude < minSwipeDistance) 
                    return Vector2.zero;

                return swipe;
            }
        }

        return Vector2.zero;

    }
    #endregion
}
