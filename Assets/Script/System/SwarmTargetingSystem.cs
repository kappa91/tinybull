﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

[UpdateAfter(typeof(PopulateSwarmSystem))]
public class SwarmTargetingSystem : SystemBase
{
    protected override void OnStartRunning()
    {
        Entities.WithAll<SingleElementSwarmTag>().ForEach((ref MoveData moveData) => {

            //Config swarm with random speed and random offset to player
            moveData.Speed = UnityEngine.Random.Range(1f, 3f);
            moveData.RandomDirection.x = UnityEngine.Random.Range(-1.5f, 1.5f);
            moveData.RandomDirection.y = UnityEngine.Random.Range(-1.5f, 1.5f);

        }).Run();
    }

    protected override void OnUpdate()
    {

        var deltaTime = Time.DeltaTime;
        var allTranslation = GetComponentDataFromEntity<Translation>(true);

        Entities.WithAll<SingleElementSwarmTag>().WithReadOnly(allTranslation).ForEach((ref MoveData moveData, in Translation translation, in TargetData targetData) => {

            //Get player translation
            Translation targetTranslation = allTranslation[targetData.Value];

            //Apply random offset to target to follow
            float3 target = new float3 ( targetTranslation.Value.x + moveData.RandomDirection.x, targetTranslation.Value.y + moveData.RandomDirection.y, 0 );
            var direction = (target - translation.Value);

            //Set Direction
            moveData.Direction = direction;
            
        }).ScheduleParallel(); 

    }
}