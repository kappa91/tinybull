﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

public class PopulateSwarmSystem : SystemBase
{
    #region SystemFlow
    protected override void OnCreate()
    {
        RequireSingletonForUpdate<GameConfigData>();
    }

    protected override void OnUpdate()
    {
        //Game design ideas
        //Use populate in game to refill the swarm after a powerup or after x seconds
    }

    protected override void OnStartRunning()
    {
        EntityQuery query = GetEntityQuery(typeof(GameConfigData));
        if (query == default) return;
        var configEntity = query.GetSingletonEntity();
        var gameConfigData = GetComponent<GameConfigData>(configEntity);

        Populate(gameConfigData);
        SetForce(configEntity, gameConfigData);
    }
    #endregion

    private void Populate(GameConfigData gameConfigData)
    {
        for (int i = 0; i < gameConfigData.NumberOfSwarm; i++)
        {
            Entity singleElementSwarm = EntityManager.Instantiate(gameConfigData.SingleElementSwarm);
            EntityManager.SetComponentData(singleElementSwarm, new TargetData { Value = gameConfigData.Player });

        }
    }


    private void SetForce(Entity configEntity, GameConfigData gameConfigData)
    {
        var force = gameConfigData.Mass * gameConfigData.NumberOfSwarm * gameConfigData.Speed;
        gameConfigData.Force = force;
        EntityManager.SetComponentData(configEntity, gameConfigData);
    }


}