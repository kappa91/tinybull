﻿using System.Collections.Generic;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

public class SpawnObstaclesSystem : SystemBase
{
    private float timer, countdown = 0;
    private SpawnObstaclesConfigData configData;
    private EntityCommandBufferSystem _buffer;


    #region SystemFlow
    protected override void OnCreate()
    {
        RequireSingletonForUpdate<SpawnObstaclesConfigData>();
    }


    protected override void OnStartRunning()
    {
        //Get config
        EntityQuery query = GetEntityQuery(typeof(SpawnObstaclesConfigData));
        if (query == default) return;
        var configEntity = query.GetSingletonEntity();
        configData = GetComponent<SpawnObstaclesConfigData>(configEntity);

        //Set spawn timer
        timer = UnityEngine.Random.Range(configData.SpawnTimerMin, configData.SpawnTimerMax);

        //Set buffer
        _buffer = World.GetOrCreateSystem<EntityCommandBufferSystem>();
    }

    protected override void OnUpdate()
    {

        var deltaTime = Time.DeltaTime;
        
        if(countdown < timer)
        {
            countdown += deltaTime;
        }
        else
        {
            SpawnObstacle();
            countdown = 0;
            timer = UnityEngine.Random.Range(configData.SpawnTimerMin, configData.SpawnTimerMax);
        }


        //Destroy obstacles out of the screen
        var tempBuffer = _buffer.CreateCommandBuffer();

        Entities.WithAll<ObstacleTag>().ForEach((Entity entity, in Translation translation) => {

            if (translation.Value.x < -10)
            {
                tempBuffer.DestroyEntity(entity);
            }

        }).Schedule();


    }
    #endregion


    private void SpawnObstacle()
    {
        Entity spawnedEntity = EntityManager.Instantiate(configData.Obstacle);

        //Set Position
        EntityManager.SetComponentData(spawnedEntity, new Translation
        {
            Value = new float3(
            configData.PositionX,
            UnityEngine.Random.Range(configData.PositionYMin, configData.PositionYMax),
            0)
        });


        //Set Mass
        var mass = UnityEngine.Random.Range(configData.MassMin, configData.MassMax);

        World.EntityManager.SetComponentData(spawnedEntity, new ObstacleData
        {
            Mass = mass
        });


        //Set Size
        float size = math.ceil((mass * (float)configData.numberOfSizes) / configData.MassMax) ;

        var currentScaleValue = GetComponent<CompositeScale>(spawnedEntity).Value.c0.x;

        float multiplyScaleValue = (currentScaleValue * size);

        World.EntityManager.SetComponentData(spawnedEntity, new CompositeScale
        {
            Value = new float4x4(multiplyScaleValue, 0, 0, 0, 0, multiplyScaleValue, 0, 0, 0, 0, multiplyScaleValue, 0, 0, 0, 0, 1)
        });
    }
}
