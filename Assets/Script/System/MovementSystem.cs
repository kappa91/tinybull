﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

public class MovementSystem : SystemBase
{
    protected override void OnUpdate()
    {
        var deltaTime = Time.DeltaTime;

        Entities.ForEach((ref Translation translation, in MoveData moveData) => {

            if (moveData.UseDeltaTime)
                translation.Value += moveData.Direction * moveData.Speed * deltaTime;
            else
                translation.Value += moveData.Direction * moveData.Speed;

        }).Schedule();
    }
}
